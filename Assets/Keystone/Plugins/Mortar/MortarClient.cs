﻿using UnityEngine;

namespace Mortar
{
    public sealed class MortarClient : MonoBehaviour
    {
        [SerializeField] private bool _offline = false;

        private void RequestMortarClient()
        {
            var instance = GetMortarInstance(_offline);
            SendMessage("SetMortarInstance", instance);
        }

        private static MortarInstance GetMortarInstance(bool offline)
        {
            if (offline) return new MortarOffline();
            return new MortarConnected();
        }
    }
}