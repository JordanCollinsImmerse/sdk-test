﻿var WebMortar = {
    $WebMortarService: {
    	OnApiMessageCallback: null,
    	OnGhostMessageCallback: null,
        OnBinaryMessageCallback: null
    },

    RegisterApiMessageCallback: function(callback) {
        WebMortarService.OnApiMessageCallback = callback;
    },
 
    RegisterGhostMessageCallback: function(callback) {
        WebMortarService.OnGhostMessageCallback = callback;
    },

    RegisterBinaryMessageCallback: function(callback) {
        WebMortarService.OnBinaryMessageCallback = callback;
    },

    SendApiMessage: function(type_ptr, args_ptr) {
    	// Convert type pointer to string.
    	var type = Pointer_stringify(type_ptr);

    	// Convert args pointer to string.
    	var args_str = Pointer_stringify(args_ptr);
    	if (args_str && args_str != "") {
    		var args = JSON.parse(args_str);
    	} else {
    		var args = [];
    	}

        // Call API message handler.
        window.OnUnityApiMessage(type, args);
    },

    SendGhostMessage: function(message_ptr, size) {
    	// Extract message byte array from the emscripten heap.
        var message = HEAPU8.slice(message_ptr, message_ptr + size);

        // Call Ghost message handler.
        window.OnUnityGhostMessage(message);
    }

};
 
autoAddDeps(WebMortar, "$WebMortarService");
mergeInto(LibraryManager.library, WebMortar);
