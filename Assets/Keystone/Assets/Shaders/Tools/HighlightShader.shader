Shader "Hidden/Highlight" {
     Properties {
         _OutlineColor ("Outline Color", Color) = (0,0,0,1)
         _Outline ("Outline width", Range (.002, 0.03)) = .005
        
        _RimPower("Highlight Rim Power", Range(0.5,8.0)) = 3.0
        _Overlay("Overlay", 2D) = "black" {}
        _ScaleX("Overlay Scale X", Float) = 1
        _ScaleY("Overlay Scale Y", Float) = 1

     }
     
	 CGINCLUDE
	 #include "UnityCG.cginc"
	 
	 struct appdata {
	     float4 vertex : POSITION;
	     float3 normal : NORMAL;
	 };
 
	 struct v2f {
	     float4 pos : POSITION;
	     float4 color : COLOR;
	 };
 
	 uniform float _Outline;
	 uniform float4 _OutlineColor;
	 
        fixed _RimPower;
        sampler2D _Overlay;
        fixed _ScaleX;
        fixed _ScaleY;

 
	 v2f vert(appdata v) {
	     // just make a copy of incoming vertex data but scaled according to normal direction
	     v2f o;
	     o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
	 
	     float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
	     float2 offset = TransformViewToProjection(norm.xy);
	 
	     o.pos.xy += offset * o.pos.z * _Outline;
	     o.color = _OutlineColor;
	     return o;
	 }
	 ENDCG
 
     SubShader {
         Tags {"Queue" = "Overlay" }
         
             Blend One One
             
		 CGPROGRAM
		 #pragma surface surf Lambert alpha
		 
		 fixed4 _Color;
		 
		 struct Input {
		    float4 screenPos;
            float3 viewDir;
            float4 _Time;

		 };
		 
		 void surf (Input IN, inout SurfaceOutput o) {
		    half rim = 1 - saturate(dot(normalize(IN.viewDir), o.Normal));
            
            float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
            screenUV.x *= _ScaleX;
            screenUV.y *= _ScaleY;
            fixed3 overlay = tex2D(_Overlay, screenUV * 50 + _Time.y*0.4).rgb;
            
            o.Alpha = ((_OutlineColor.rgb * (pow(rim, _RimPower)) + overlay + (0.05 * rim)) * _OutlineColor.a);
            o.Emission = o.Alpha;
		 }
		 ENDCG
 
         // note that a vertex shader is specified here but its using the one above
         Pass {
             Name "OUTLINE"
             Tags { "LightMode" = "Always" "Queue" = "Overlay" }
             Cull Front
             ZWrite Off
             ZTest LEqual
             ColorMask RGB
             Blend SrcAlpha OneMinusSrcAlpha
             Offset 15,15
 
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             half4 frag(v2f i) :COLOR { return i.color; }
             ENDCG
         }
     }
     
     Fallback "Diffuse"
 }