﻿Shader "Hidden/VR Head" {
	Properties {
		_Color("Color", Color) = (1,1,1,1)
		_AnimationSpeed ("Animation Speed", Range(0, 20)) = 1
		_Overlay ("Overlay", 2D) = "white" {}
		_OverlayScale("Overlay Scale", Float) = 2		
		_Brightness ("Brightness", Range(0, 1)) = 0.5
				
		_RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
		_RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" "IgnoreProjector"="True" "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert 

		fixed4 _Color;
		sampler2D _Overlay;
		fixed _OverlayScale;
		
		float _Brightness;
		float _AnimationSpeed;

		  float4 _RimColor;
		  float _RimPower;

		struct Input {
			float4 _Time;
          float4 screenPos;
          float3 viewDir;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = _Color;
			float2 t = float2(0,_Time.x * _AnimationSpeed);

			fixed2 uv =  IN.screenPos.xy / IN.screenPos.w * _OverlayScale;
			half4 overlay = tex2D (_Overlay, uv + t);
			
			o.Alpha = c.a;
	
          half rim = 1.2 - saturate(dot (normalize(IN.viewDir), o.Normal));
          o.Emission = _RimColor.rgb * pow (rim, _RimPower);

			o.Emission += (c.rgb * c.a) * _Brightness*overlay.rbg;
			o.Albedo = c.rgb;
			
		}
		ENDCG
	} 
	FallBack "Diffuse"
}