Shader "Unlit/Texture + Color" {
	Properties {
		_Color ("Tint", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	}
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 100
		
		ZWrite On
//		Blend SrcAlpha OneMinusSrcAlpha 
	
		Pass {
			Lighting Off
			SetTexture [_MainTex] { 
				constantColor[_Color]
				combine constant * texture 
			} 
		}
	}
}

// Test comment