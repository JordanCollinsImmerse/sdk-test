﻿using Valve.VR;

namespace Keystone.Avatars.Controllers
{
    public class SteamVRInput : IInputModule
    {
        private const EVRButtonId EngageButtonId = EVRButtonId.k_EButton_SteamVR_Trigger;
        private const EVRButtonId PointButtonId = EVRButtonId.k_EButton_ApplicationMenu;

        private readonly SteamVR_Controller.Device _device;

        public SteamVRInput(SteamVR_TrackedObject trackedObject)
        {
            var deviceIndex = (int) trackedObject.index;
            _device = SteamVR_Controller.Input(deviceIndex);
        }

        public bool GetButtonUp(Buttons button)
        {
            switch (button)
            {
                case Buttons.Engage:
                    return _device.GetTouchUp(EngageButtonId);
                case Buttons.Point:
                    return _device.GetTouchUp(PointButtonId);
                default:
                    return false;
            }
        }

        public bool GetButtonDown(Buttons button)
        {
            switch (button)
            {
                case Buttons.Engage:
                    return _device.GetTouchDown(EngageButtonId);
                case Buttons.Point:
                    return _device.GetTouchDown(PointButtonId);
                default:
                    return false;
            }
        }

        public float GetAxis(Buttons button)
        {
            switch (button)
            {
                case Buttons.Engage:
                    return _device.GetAxis(EVRButtonId.k_EButton_Axis1).x;
                default:
                    return 0;
            }
        }

        public void Vibrate(ushort durationMicroSec)
        {
            _device.TriggerHapticPulse(durationMicroSec);
        }

        public override string ToString()
        {
            return string.Format("SteamVR Input Module, {0}", _device.index);
        }
    }
}